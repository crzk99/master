#!/bin/bash
#titulo:            INSTALADOR DE GENVPSMX
#descripcion:  Generador de KEYS  
#author:           @sixReaper
#created:          Septiembre 11 2020
#updated:         Septiembre 12 2020   
#licencia:          Script Público // Gratuito
#==========================================================================
if [ $(id -u) != 0 ]
then
echo "Ejecute el script como root"
exit
fi
rm -Rf /etc/SCRIPT/* /usr/bin/gerar* /usr/bin/http-server.py* /usr/bin/http-server.sh* /root/name* /usr/bin/master-server.py
clear
BARRA="\033[1;37m-----------------------------------------------------\033[0m"
IVAR="/etc/http-instas"
SCPT_DIR="/etc/SCRIPT"
DOMG="aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL3JlYXBlcmdhbW8vZ2Ftby9tYXN0ZXIvZ2VyYWRvcg=="
DDOMG='base64 -d'
rm $(pwd)/$0

veryfy_fun () {
[[ ! -d ${IVAR} ]] && touch ${IVAR}
[[ ! -d ${SCPT_DIR} ]] && mkdir ${SCPT_DIR}
unset ARQ
case $1 in
"master.sh")ARQ="/usr/bin/";;
"master-server.py")ARQ="/bin/";;
*)ARQ="${SCPT_DIR}/";;
esac
mv -f $HOME/$1 ${ARQ}/$1
chmod +x ${ARQ}/$1
}
meu_ip () {
MIP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
MIP2=$(wget -qO- ipv4.icanhazip.com)
[[ "$MIP" != "$MIP2" ]] && IP="$MIP2" || IP="$MIP"
echo "$IP" > /usr/bin/vendor_code
}
meu_ip
clear
echo -e "$BARRA"
echo -e "\033[1;33mＧＥＮＥＲＡＤＯＲ ＶＰＳ-ＭＸ                    "
echo -e "$BARRA"
cd $HOME
REQUEST=$(echo $DOMG|$DDOMG)
wget -O "$HOME/lista-arq" ${REQUEST}/GERADOR > /dev/null 2>&1
sleep 1s
[[ -e $HOME/lista-arq ]] && {
for arqx in `cat $HOME/lista-arq`; do
echo -ne "\033[1;33mObteniendo: \033[1;37m[$arqx] "
wget -O $HOME/$arqx ${REQUEST}/${arqx} > /dev/null 2>&1 && {
echo -e "\033[1;31m- \033[1;32mRecibido!"
[[ -e $HOME/$arqx ]] && veryfy_fun $arqx
} || echo -e "\033[1;31m- \033[1;31mFalla (no recibido!)"
done
[[ ! -e /usr/bin/trans ]] && wget -O /usr/bin/trans https://raw.githubusercontent.com/rudi9999/Generador_Gen_VPS-MX/master/Install/trans &> /dev/null
[[ -e /bin/master-server.py ]] && mv -f /bin/master-server.py /bin/http-server.sh && chmod +x /bin/http-server.sh
[[ $(dpkg --get-selections|grep -w "bc"|head -1) ]] || apt-get install bc -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "screen"|head -1) ]] || apt-get install screen -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "nano"|head -1) ]] || apt-get install nano -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || apt-get install curl -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || apt-get install figlet -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || apt-get install netcat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "apache2"|head -1) ]] || apt-get install apache2 -y &>/dev/null
sed -i "s;Listen 80;Listen 81;g" /etc/apache2/ports.conf
service apache2 restart > /dev/null 2>&1 &
IVAR2="/etc/key-gerador"
echo "$Key" > $IVAR
rm $HOME/lista-arq
sed -i -e 's/\r$//' /usr/bin/master.sh
echo -e "$BARRA"
echo "/usr/bin/master.sh" > /usr/bin/master && chmod +x /usr/bin/master
clear
echo -e "$BARRA"
echo -e "\033[1;32m Personalizacion - Escribe tu Nick:\033[0m"
echo -e "\033[1;32m (Maximo 10 Caracteres)\033[0m"
echo -e "$BARRA"
read -p ": " nickname
echo "$nickname" >> /root/name
rm -rf g
clear
echo -e "$BARRA"
echo -e "\033[1;33mBien, utilize el comando \033[1;37m [ master.sh o master ]
\033[1;33mpara administrar sus keys"
echo -e "$BARRA"
} || {
echo -e "$BARRA"
echo -e "\033[1;33mKey Invalida!"
echo -e "$BARRA"
}
echo -ne "\033[0m"
apt-get install netcat -y &>/dev/null
apt-get install net-tools -y &>/dev/null
sed -i -e 's/\r$//' /usr/bin/master.sh